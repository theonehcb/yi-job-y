/*
Navicat MySQL Data Transfer

Source Server         : aliyun
Source Server Version : 50738
Source Host           : 101.132.167.26:3306
Source Database       : yi-job

Target Server Type    : MYSQL
Target Server Version : 50738
File Encoding         : 65001

Date: 2022-11-18 10:32:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for delivery_record
-- ----------------------------
DROP TABLE IF EXISTS `delivery_record`;
CREATE TABLE `delivery_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243384322 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of delivery_record
-- ----------------------------
INSERT INTO `delivery_record` VALUES ('-1354756094', '5', '5', '2022-11-11 17:02:08', '0');
INSERT INTO `delivery_record` VALUES ('3', '1', '1', '2022-10-31 19:17:34', '2');
INSERT INTO `delivery_record` VALUES ('4', '1', '2', '2022-10-10 17:51:07', '2');
INSERT INTO `delivery_record` VALUES ('5', '5', '1', '2022-10-27 20:12:13', '-1');
INSERT INTO `delivery_record` VALUES ('6', '5', '2', '2022-10-30 21:56:28', '2');
INSERT INTO `delivery_record` VALUES ('243384321', '5', '3', '2022-11-10 10:44:26', '0');

-- ----------------------------
-- Table structure for interview
-- ----------------------------
DROP TABLE IF EXISTS `interview`;
CREATE TABLE `interview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `site` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of interview
-- ----------------------------
INSERT INTO `interview` VALUES ('1', '2', '5', '2022-10-31 00:00:00', '线上');
INSERT INTO `interview` VALUES ('2', '1', '1', '2022-11-01 00:00:00', '人民广场');

-- ----------------------------
-- Table structure for job
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `salary` varchar(10) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `scale` varchar(255) DEFAULT NULL COMMENT '规模',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job
-- ----------------------------
INSERT INTO `job` VALUES ('1', 'java开发', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', '易求职有限公司', '6K-8K', '成都', '1年以上', '本科', '100人');
INSERT INTO `job` VALUES ('2', 'java实习', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', '易求职有限公司', '4K-6K', '成都', '无', '研究生', '50人');
INSERT INTO `job` VALUES ('3', 'java初级', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', '京东有限公司', '3K-4K', '成都', '无', '大专', '300-500人');
INSERT INTO `job` VALUES ('4', 'java开发', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', '谷歌有限公司', '4K-6K', '成都', '3年以上', '高中', '100人');
INSERT INTO `job` VALUES ('5', 'java开发', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', 'ABC有限公司', '4K-6K', '成都', '无', '研究生', '300-500人');
INSERT INTO `job` VALUES ('6', 'java研发', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', '易求职有限公司', '6K-8K', '成都', '1年以上', '本科', '100人');
INSERT INTO `job` VALUES ('7', 'java开发', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', 'Nav有限公司', '10K-14K', '成都', '1年以上', '本科', '50人');
INSERT INTO `job` VALUES ('8', 'java开发', 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', '易求职有限公司', '4K-6K', '成都', '无', '本科', '100人');
INSERT INTO `job` VALUES ('9', '扫大街', null, '五谷', '1000', '厕所', null, '研究生', null);

-- ----------------------------
-- Table structure for job_detail
-- ----------------------------
DROP TABLE IF EXISTS `job_detail`;
CREATE TABLE `job_detail` (
  `job_id` int(11) NOT NULL,
  `boss_id` int(11) DEFAULT NULL,
  `job_describe` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job_detail
-- ----------------------------
INSERT INTO `job_detail` VALUES ('1', '2', '具备良好的计算机、软件工程等专业领域基础知识；\r\n3、具备云计算、高性能计算相关领域开发维护相关经验；', '公司在广州、成都设立研发中心，核心技术人员均来自于国内知名互联网和金融行业优秀企业。现有员工200人，其中技术团队占公司人数75%以上，具有强大的研发创新能力');

-- ----------------------------
-- Table structure for job_tags
-- ----------------------------
DROP TABLE IF EXISTS `job_tags`;
CREATE TABLE `job_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tags_id` int(11) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job_tags
-- ----------------------------
INSERT INTO `job_tags` VALUES ('1', '1', 'java');
INSERT INTO `job_tags` VALUES ('4', '2', '分布式');
INSERT INTO `job_tags` VALUES ('5', '2', 'redis');
INSERT INTO `job_tags` VALUES ('6', '2', '消息队列');
INSERT INTO `job_tags` VALUES ('7', '3', '多线程');
INSERT INTO `job_tags` VALUES ('8', '3', '消息队列');
INSERT INTO `job_tags` VALUES ('9', '3', 'springboot');
INSERT INTO `job_tags` VALUES ('13', '1', 'mysql');
INSERT INTO `job_tags` VALUES ('15', null, '香');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) DEFAULT NULL,
  `user_to` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2118127619 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('-1774186494', '1', '2', '不能说的秘密', '2022-09-20 20:23:03');
INSERT INTO `message` VALUES ('-1543475198', '1', '3', '草色入帘青', '2022-09-18 09:03:01');
INSERT INTO `message` VALUES ('-1514115071', '1', '2', '小失误', '2022-09-18 10:04:01');
INSERT INTO `message` VALUES ('-1375727614', '5', '1', '你好', '2022-10-27 20:39:26');
INSERT INTO `message` VALUES ('-591282174', '2', '1', '还是罢了', '2022-09-20 15:56:49');
INSERT INTO `message` VALUES ('-4165631', '1', '3', 'fake', '2022-09-18 09:48:24');
INSERT INTO `message` VALUES ('163672067', '1', '3', 'he', '2022-09-12 09:59:53');
INSERT INTO `message` VALUES ('163672068', '2', '3', 'ok', '2022-09-12 10:00:26');
INSERT INTO `message` VALUES ('163672069', '4', '1', 'jk', '2022-09-12 10:08:40');
INSERT INTO `message` VALUES ('494956546', '1', '4', 'i like', '2022-09-18 10:02:52');
INSERT INTO `message` VALUES ('570454018', '1', '3', '等待', '2022-09-18 09:50:08');
INSERT INTO `message` VALUES ('1489006594', '1', '3', '苔痕上阶绿', '2022-09-18 09:46:36');
INSERT INTO `message` VALUES ('1635794946', '1', '2', '收到', '2022-09-18 22:20:16');
INSERT INTO `message` VALUES ('1644216322', '1', '2', '你好', '2022-09-17 14:53:52');
INSERT INTO `message` VALUES ('2109763585', '1', '3', '谈笑有鸿儒', '2022-09-18 09:47:10');
INSERT INTO `message` VALUES ('2118127618', '2', '1', '轨迹', '2022-09-20 20:25:43');

-- ----------------------------
-- Table structure for publish
-- ----------------------------
DROP TABLE IF EXISTS `publish`;
CREATE TABLE `publish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `boss_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of publish
-- ----------------------------
INSERT INTO `publish` VALUES ('1', '1', '2');
INSERT INTO `publish` VALUES ('2', '2', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `biographical_notes_url` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'hcb', '123456', '123456', 'https://i2.hdslb.com/bfs/face/94e219c00d551dc25cdddb3686ae96df367eabb8.jpg@240w_240h_1c_1s.webp', 'https://hcb-yi-job.oss-cn-hangzhou.aliyuncs.com/010592b9e8d04d5f8badded417964b19=java简历.docx', null);
INSERT INTO `user` VALUES ('2', 'boss', '654321', '654321', 'https://img.bosszhipin.com/beijin/upload/com/logo/20220607/7b5b554d84f9729c2597b95b4db45cecb444a02ba5abfe5297c836ca124517b584d9d9102a331172.jpg?x-oss-process=image/resize,w_100,limit_0', 'https://hcb-yi-job.oss-cn-hangzhou.aliyuncs.com/41ea2b8ec18b40818248f784fc24b3a1=java简历.pdf', null);
INSERT INTO `user` VALUES ('3', 'boss2', null, null, 'https://img.bosszhipin.com/beijin/mcs/banner/fff9b16bcbb78e2453884865d2c4c7e7cfcd208495d565ef66e7dff9f98764da.jpg?x-oss-process=image/resize,w_120,limit_0', null, null);
INSERT INTO `user` VALUES ('4', 'boss3', null, null, 'https://img.bosszhipin.com/beijin/upload/com/logo/20200429/8f64c9d0b30a53c434b26799ca78966cec9b49ecaf1604cb263748f03d5c2b0f.png?x-oss-process=image/resize,w_120,limit_0', null, null);
INSERT INTO `user` VALUES ('5', '南一', '1666407902751', '123456', 'https://hcb-yi-job.oss-cn-hangzhou.aliyuncs.com/55a5756e19c842288fa2c7221be77d2a=avater.jpg', 'https://hcb-yi-job.oss-cn-hangzhou.aliyuncs.com/5dcacde1d4354b24a293b1172e3abfaa=java简历.pdf', '18784993477');
INSERT INTO `user` VALUES ('6', '用户15567564535', '1666408085595', '123456', null, null, '15567564535');
