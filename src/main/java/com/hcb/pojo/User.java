package com.hcb.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
public class User {
    @TableId(type = IdType.AUTO)
    Long id;
    String name;
    String account;
    String password;
    String imgUrl;
    String biographicalNotesUrl;
    @TableField(value = "phoneNumber")
    String phoneNumber;
}
