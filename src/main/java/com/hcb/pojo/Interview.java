package com.hcb.pojo;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 11314
 * @description TODO
 * @date 2022/10/28 21:33
 */
@Data
public class Interview {
    Integer jobId;
    Integer userId;
    Timestamp date;
    String site;
}
