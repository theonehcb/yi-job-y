package com.hcb.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class Message {
    @TableId
    Integer id;
    Integer userFrom;
    Integer userTo;
    String message;
    Timestamp date;
}
