package com.hcb.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("job_tags")
public class Tags {
    @TableId(type = IdType.AUTO)
    Long id;
    Long tagsId;
    String tag;
}