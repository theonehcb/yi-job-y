package com.hcb.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class JobDetail {
    @TableId
    Long jobId;
    Long bossId;
    String jobDescribe;
    String introduce;
}
