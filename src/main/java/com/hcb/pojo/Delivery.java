package com.hcb.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 11314
 * @description 用户job投递记录
 * @date 2022/9/29 9:36
 */
@Data
@TableName("delivery_record")
public class Delivery {
    Integer id;
    Integer userId;
    Integer jobId;
    Timestamp date;
    Integer state;
}
