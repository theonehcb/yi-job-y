package com.hcb.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.*;

@Data
public class Job{
    @TableId(type = IdType.AUTO)
    Long jobId;
    String name;
    String logo;
    String companyName;
    String salary;
    String location;
    String experience;
    String degree;
    String scale;
    @TableField(exist = false)
    List<Tags> tags;
}
