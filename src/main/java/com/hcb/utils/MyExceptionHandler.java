package com.hcb.utils;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 11314
 * @description TODO
 * @date 2022/9/29 10:06
 */
@ControllerAdvice
public class MyExceptionHandler{
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public R handleException(Exception e){
        e.printStackTrace();
        return R.failure(e.getMessage());
    }
}
