package com.hcb.utils;

import java.io.Serializable;

public class R<T> implements Serializable {
    private String msg;

    private String status;

    private T data;

    public R(){}
    public R(String msg, String status, T data) {
        this.msg = msg;
        this.status = status;
        this.data = data;
    }
    public R(String msg,String status){
        this.msg=msg;
        this.status=status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    public static <T> R<T> success(T data){
        R result=new R();
        result.setData(data);
        result.setStatus("200");
        return result;
    }

    public static <T> R<T> failure(String msg){
        R result=new R(msg,"300",msg);
        return result;
    }
}
