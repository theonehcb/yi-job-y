package com.hcb.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
    public static SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy年MM月dd日 hh:mm");
    public static String formatDate(Date date){
       return simpleDateFormat.format(date);
    }
}
