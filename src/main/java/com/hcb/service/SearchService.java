package com.hcb.service;

import java.util.List;
import java.util.Set;

public interface SearchService {
    Set<String> getHotSearch();
    List<String> getRecentSearch();
    void addSearchRecord(String search);
}
