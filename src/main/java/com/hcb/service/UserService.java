package com.hcb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hcb.pojo.Delivery;
import com.hcb.pojo.User;
import com.hcb.vo.DeliverVo;
import com.hcb.vo.InterviewList;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface UserService{
    User getUser();
    public String login(String account,String password);
    public boolean deliver(Delivery delivery);
    public int getDeliverNum(int userId);
    public List<DeliverVo> getDelivervo(int userId);
    public String changeAvater(MultipartFile file,Integer id);
    public void changeName(User user);
    public String uploadFile(MultipartFile file,Integer id);
    public String getPassword(String phoneNumber);
    public String regist(User user);
    //投递信息 投递数、通过率、反馈记录
    public Map<String,Object> getDeliverInfo(Integer id);
    //面试列表
    public List<InterviewList> getInterviews(int id);
    List<String> getRecentSearch();
    void addSearchRecord(String search);
}
