package com.hcb.service.delivery;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hcb.pojo.Delivery;
import com.hcb.vo.ApplyVo;

import java.util.List;
import java.util.Map;

public interface DeliveryService extends IService<Delivery> {
    /**
     * 获取当前用户投递的jobid
     * @return
     */
    List<Long> getDeliverId();

    List<Map<String, Object>> getPassApplyVo(int bossId);

    List<ApplyVo> getApplyVo(int bossId);
}
