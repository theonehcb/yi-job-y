package com.hcb.service.delivery;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hcb.mapper.DeliveryMapper;
import com.hcb.pojo.Delivery;
import com.hcb.service.impl.LoginTreadLocalService;
import com.hcb.vo.ApplyVo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 11314
 * @description TODO
 * @date 2022/11/9 21:44
 */
@Service
public class DeliveryImpl extends ServiceImpl<DeliveryMapper, Delivery> implements DeliveryService{
    @Autowired
    LoginTreadLocalService loginTreadLocalService;
    @Override
    public List<Long> getDeliverId() {
            return baseMapper.getDeliveryId(loginTreadLocalService.getUserId());
    }

    @Override
    public List<Map<String, Object>> getPassApplyVo(int bossId) {
        return baseMapper.getPassApplyVo(bossId);
    }

    @Override
    public List<ApplyVo> getApplyVo(int bossId) {
        return baseMapper.getApplyVo(bossId);
    }
}
