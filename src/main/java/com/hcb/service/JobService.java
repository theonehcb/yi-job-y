package com.hcb.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hcb.pojo.Interview;
import com.hcb.pojo.Job;
import com.hcb.pojo.JobDetail;
import com.hcb.vo.ApplyVo;
import com.hcb.vo.JobVo;

import java.util.List;
import java.util.Map;

public interface JobService extends IService<Job> {
     JobDetail getJobDetail(int id);
     Page<JobVo> getJob(int num, String salary, String exp, String degree, String scale, String input);
     List<ApplyVo> getApplyVo(int bossId);
     List<Map<String,Object>> getPassApplyVo(int bossId);
     List<Job> getpublish(int bossId);
     void check(int state,int id);
     void interview(Interview interview);
    //更新
     boolean updateJob(Job job);
    //发布
     void publish(Job job,int bid);
}
