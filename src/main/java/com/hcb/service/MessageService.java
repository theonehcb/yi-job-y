package com.hcb.service;

import com.hcb.pojo.Message;
import com.hcb.vo.ChartVo;
import com.hcb.vo.MessageVo;

import java.util.List;

public interface MessageService {
    public List<MessageVo> getMessagesList(Integer userId);
    public List<ChartVo> getChartList(Integer userId, Integer userId2);
    public void sendMessage(Message message);
}
