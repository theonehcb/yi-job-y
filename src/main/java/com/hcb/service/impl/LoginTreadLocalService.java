package com.hcb.service.impl;

import org.springframework.stereotype.Service;

/**
 * @author 11314
 * @description TODO
 * @date 2022/11/4 19:07
 */
@Service
public class LoginTreadLocalService {
   private final ThreadLocal<Integer> userIdThreadLocal=new ThreadLocal<>();
   public  int getUserId(){
       return userIdThreadLocal.get();
   }
   public void setUserId(int id){
       userIdThreadLocal.set(id);
   }
   public void remove(){
       userIdThreadLocal.remove();
   }
}
