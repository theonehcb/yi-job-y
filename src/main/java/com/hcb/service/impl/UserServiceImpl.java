package com.hcb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hcb.mapper.DeliveryMapper;
import com.hcb.mapper.UserMapper;
import com.hcb.pojo.Delivery;
import com.hcb.pojo.User;
import com.hcb.service.SearchService;
import com.hcb.service.UserService;
import com.hcb.utils.JwtUtil;
import com.hcb.utils.aliyunoss;
import com.hcb.vo.DeliverVo;
import com.hcb.vo.InterviewList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@ConditionalOnProperty(prefix = "config.redis",name = "enable",havingValue = "false")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    LoginTreadLocalService loginTreadLocalService;

    @Autowired
    DeliveryMapper deliveryMapper;
    @Autowired
    aliyunoss aliyun;
    @Autowired
    SearchService searchService;
    @Override
    public User getUser() {
        return baseMapper.selectById(loginTreadLocalService.getUserId());
    }

    @Override
    public String login(String account,String password) {
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getAccount,account)
                .eq(User::getPassword,password);
        queryWrapper.or(q->q.eq(User::getPhoneNumber,account).eq(User::getPassword,password));
        User user=baseMapper.selectOne(queryWrapper);
        if(user!=null){
            return JwtUtil.buildToken(user);
        }
        return "";
    }

/**
 * 投递简历
 */
    @Override
    public boolean deliver(Delivery delivery) {
        delivery.setDate(new Timestamp(System.currentTimeMillis()));
        deliveryMapper.insert(delivery);
        return true;
    }
    /**
     *   投递数量
     */
    @Override
    public int getDeliverNum(int userId) {
       return deliveryMapper.selectCount(new LambdaQueryWrapper<Delivery>().eq(Delivery::getUserId,userId)).intValue();
    }
    @Override
    public List<DeliverVo> getDelivervo(int userId) {
       return baseMapper.getDeliverVoList(userId);
    }


    @Override
    public String changeAvater(MultipartFile file,Integer id) {
        String url=aliyun.uploadimg(file);
        update(new LambdaUpdateWrapper<User>().eq(User::getId,id).set(User::getImgUrl,url));
        return url;
    }

    @Override
    public void changeName(User user) {
        updateById(user);
    }

    @Override
    public String getPassword(String phoneNumber) {
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getPhoneNumber,phoneNumber)
        .select(User::getPassword);
        User user=getOne(queryWrapper);
        if(user==null)throw new RuntimeException("号码错误或未绑定该手机号");
        return user.getPassword();
    }

    @Override
    public String regist(User user) {
        String number=user.getPhoneNumber();
        if(getOne(new LambdaQueryWrapper<User>().eq(User::getPhoneNumber,number))!=null){
            throw new RuntimeException("该手机号已被注册");
        }else {
            user.setName("用户"+number);
           String account=String.valueOf(System.currentTimeMillis());
           account.substring(account.length()-10);
            user.setAccount(account);
            save(user);
        }
        return "注册成功";
    }

    @Override
    public String uploadFile(MultipartFile file,Integer id) {
        String url=aliyun.uploud(file);
        update(new LambdaUpdateWrapper<User>().eq(User::getId,id).set(User::getBiographicalNotesUrl,url));
        return url;
    }

    /**
     * 投递信息 投递数、反馈记录
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getDeliverInfo(Integer id) {
        //投递数
        int count=getDeliverNum(id);
        //记录
        List<Map<String, Object>> deliveries=deliveryMapper.feedback(id);
        Map<String, Object> map=new HashMap<>();
        map.put("count",count);
        map.put("record",deliveries);
        return map;
    }

    @Override
    public List<InterviewList> getInterviews(int id) {
        return baseMapper.getInterviews(id);
    }

    @Override
    public List<String> getRecentSearch() {
        return searchService.getRecentSearch();
    }

    @Override
    public void addSearchRecord(String search) {
       searchService.addSearchRecord(search);
    }

}

