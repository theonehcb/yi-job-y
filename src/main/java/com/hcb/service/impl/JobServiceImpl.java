package com.hcb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hcb.mapper.*;
import com.hcb.pojo.*;
import com.hcb.service.JobService;
import com.hcb.service.delivery.DeliveryService;
import com.hcb.vo.ApplyVo;
import com.hcb.vo.JobVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class JobServiceImpl extends ServiceImpl<JobMapper, Job> implements JobService {

    @Autowired
    TagsMapper tagsMapper;
    @Autowired
    JobDetailMapper jobDetailMapper;
    @Autowired
    InterviewMapper interviewMapper;
    @Autowired
    DeliveryService deliveryService;
    @Override
    public JobDetail getJobDetail(int id) {
        return jobDetailMapper.selectById(id);
    }


    public Page<JobVo> getJob(int num, String salary, String exp, String degree, String scale, String input) {
        Page<JobVo> page=new Page<>();
        Page p=getJobs(num,salary,exp,degree,scale,input);
        page.setCurrent(num);
        page.setTotal(p.getTotal());
        List<JobVo> list=toJobVoList(p.getRecords());
        page.setRecords(list);
        return page;
    }
    public Page<Job> getJobs(int num, String salary, String exp, String degree, String scale, String input) {
        LambdaQueryWrapper<Job> lambdaQueryWrapper=new LambdaQueryWrapper();
        if(!salary.equals("")){
            lambdaQueryWrapper.eq(Job::getSalary,salary);
        }if(!exp.equals("")){
            lambdaQueryWrapper.eq(Job::getExperience,exp);
        }if(!degree.equals("")){
            lambdaQueryWrapper.eq(Job::getDegree,degree);
        }if(!scale.equals("")){
            lambdaQueryWrapper.eq(Job::getScale,scale);
        }if(!input.equals("")){
            lambdaQueryWrapper.and(w->w.like(Job::getName,input).or().like(Job::getCompanyName,input));
        }
        Page<Job> jobPage=baseMapper.selectPage(new Page(num,5),lambdaQueryWrapper);
        return getJobPage(jobPage);
    }


    /**
     * 申请列表
     * @param bossId
     * @return
     */
    @Override
    public List<ApplyVo> getApplyVo(int bossId) {
        return deliveryService.getApplyVo(bossId);
    }

    /**
     * 已通过列表
     * @param bossId
     * @return
     */
    @Override
    public List<Map<String,Object>> getPassApplyVo(int bossId) {
        return deliveryService.getPassApplyVo(bossId);
    }

    @Override
    public List<Job> getpublish(int bossId) {
       List<Job> jobs=baseMapper.getpublish(bossId);
        jobs.forEach((job -> {
            LambdaQueryWrapper<Tags> wrapper=new LambdaQueryWrapper();
            wrapper.eq(Tags::getTagsId,job.getJobId());
            job.setTags(tagsMapper.selectList(wrapper));
        }));
        return jobs;
    }

    @Override
    public boolean updateJob(Job job) {
        baseMapper.updateById(job);
        job.getTags().forEach(tags -> {
            //说明是新增
            if(tags.getId()==null){
                tags.setTagsId(job.getJobId());
                tagsMapper.insert(tags);
            }
            //说明被删除
            else if(tags.getTagsId()==null){
                tagsMapper.deleteById(tags.getId());
            }
            tagsMapper.updateById(tags);
        });
        return true;
    }

    @Override
    public void publish(Job job,int bid) {
        List<Tags> tags=job.getTags();
        //插入job
        baseMapper.insert(job);
        //tag插入数据库
        tags.forEach(t->{
            t.setTagsId(job.getJobId());
            tagsMapper.insert(t);
        });
        //绑定发布者
        baseMapper.publish(bid,job.getJobId());
    }

    /**
     * 审核
     * @param state
     * @return
     */
    @Override
    public void check(int state,int id) {
        baseMapper.check(id,state);
    }
        //往jobs里注入tags
    private Page<Job> getJobPage(Page<Job> jobPage) {
        List<Job> jobs=jobPage.getRecords();
        jobs.forEach((job -> {
            LambdaQueryWrapper<Tags> wrapper=new LambdaQueryWrapper();
            wrapper.eq(Tags::getTagsId,job.getJobId());
            job.setTags(tagsMapper.selectList(wrapper));
        }));
        return jobPage;
    }

    @Override
    public void interview(Interview interview) {
        //新增面试记录
        interviewMapper.insert(interview);
        //更新投递信息
        deliveryService.update(null,new LambdaUpdateWrapper<Delivery>()
                .eq(Delivery::getUserId,interview.getUserId())
        .eq(Delivery::getJobId,interview.getJobId())
        .set(Delivery::getState,2));
    }
    List<JobVo> toJobVoList(List<Job> jobs){
        List<Long> ids=deliveryService.getDeliverId();
        System.out.println(ids);
        List<JobVo> list=new ArrayList<>();
        jobs.forEach(job -> {
            JobVo jobVo=new JobVo(job);
            jobVo.setVisited(ids.contains(job.getJobId()));
            list.add(jobVo);
        });
        return list;
    }
}
