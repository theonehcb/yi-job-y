package com.hcb.controllers;

import com.hcb.pojo.Message;
import com.hcb.service.MessageService;
import com.hcb.utils.JwtUtil;
import com.hcb.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    MessageService messageService;
    @GetMapping("/list")
    public R getMessageList(HttpServletRequest request){
        String token=request.getHeader("token");
        int id=JwtUtil.getUserId(token);
        return R.success(messageService.getMessagesList(id));
    }
    //聊天列表
    @GetMapping("/chart/{id2}")
    public R getMessageChart(HttpServletRequest request,
                             @PathVariable Integer id2){
        String token=request.getHeader("token");
        int id=JwtUtil.getUserId(token);
        return R.success(messageService.getChartList(id, id2));
    }
    @PostMapping("/send")
    public void sendMessage(@RequestBody Message message,
                            HttpServletRequest request){
        String token=request.getHeader("token");
        int id=JwtUtil.getUserId(token);
        message.setUserFrom(id);
        messageService.sendMessage(message);
    }
}
