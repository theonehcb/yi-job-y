package com.hcb.controllers;

import com.hcb.pojo.Delivery;
import com.hcb.pojo.Interview;
import com.hcb.pojo.Job;
import com.hcb.service.JobService;
import com.hcb.utils.JwtUtil;
import com.hcb.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

@RestController
@RequestMapping("/job")
public class JobController {
    @Autowired
    JobService jobService;

    @GetMapping("/detail/{id}")
    public R getDetail(@PathVariable int id){
        return R.success(jobService.getJobDetail(id));
    }

    @GetMapping("/getpage/{num}")
    public R getJob(@RequestParam(required = false)String salary,
                    @RequestParam(required = false)String exp,
                    @RequestParam(required = false)String degree,
                    @RequestParam(required = false)String scale,
                    @RequestParam String input,
                    @PathVariable int num
                    ){
        return R.success(jobService.getJob(num,salary,exp,degree,scale,input));
    }

    /**
     * // TODO: 获得未审核申请列表
     * @param request
     */
    @GetMapping("/getapply")
    public R getApply(HttpServletRequest request){
        return R.success(jobService.getApplyVo(JwtUtil.getUserId(request.getHeader("token"))));
    }

    /**
     * // TODO: 发布的职位
     * @param request
     */
    @GetMapping("/publish")
    public R getPublish(HttpServletRequest request){
        return R.success(jobService.getpublish(JwtUtil.getUserId(request.getHeader("token"))));
    }
    @PostMapping("/updateJob")
    public R updateJob(@RequestBody Job job){
        return R.success(jobService.updateJob(job));
    }
    @PostMapping("/publishjob")
    public void publishJob(@RequestBody Job job,
                           HttpServletRequest request){
        jobService.publish(job,JwtUtil.getUserId(request));
    }
    /**
     * 审核申请列表
     * @param
     * @return
     */
    @PostMapping("/check")
    public void check(@RequestBody Delivery delivery){
        jobService.check(delivery.getState(),delivery.getId());
    }
    /**
     * 已通过的申请
     */
    @GetMapping("/pass")
    public R getPass(HttpServletRequest request){
        int id=JwtUtil.getUserId(request);
        return R.success(jobService.getPassApplyVo(id));
    }
    /**
     * 约面试
     */
    @PostMapping("/interview")
    public void interview(@RequestBody Interview interview){
        jobService.interview(interview);
    }
}
