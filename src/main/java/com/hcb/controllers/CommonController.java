package com.hcb.controllers;

import com.hcb.service.SearchService;
import com.hcb.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 11314
 * @description TODO
 * @date 2022/11/7 8:46
 */
@RestController
@RequestMapping("/sys")
public class CommonController {
    @Autowired
    SearchService searchService;
    @GetMapping("/gethotsearch")
    public R getHotSearch(){
        return R.success(searchService.getHotSearch());
    }
}
