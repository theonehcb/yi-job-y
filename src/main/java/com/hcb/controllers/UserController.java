package com.hcb.controllers;

import com.hcb.pojo.Delivery;
import com.hcb.pojo.User;
import com.hcb.service.UserService;
import com.hcb.utils.JwtUtil;
import com.hcb.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService service;
    @GetMapping("/getuser")
    public User queryUserById(){
        return service.getUser();
    }
    @GetMapping("/login/{account}/{password}")
    public R login(@PathVariable String account,
                   @PathVariable String password){
        String token=service.login(account,password);
        if(token.equals("")){
            return R.failure("无该用户");
        }
        Map<String,Object> map=new HashMap<>();
        map.put("token",token);
        map.put("id",JwtUtil.getUserId(token));
        return R.success(map);
    }
    @PostMapping("/delivery")
    public R delivery(@RequestBody Delivery delivery,
                      HttpServletRequest request){
        delivery.setUserId(JwtUtil.getUserId(request.getHeader("token")));
        if(service.deliver(delivery)){
            return R.success("投递成功");
        }
        return R.failure("已投递该岗位");
    }
    /**
     * 已投递数量
     */
    @GetMapping("/deliveryNum")
    public R getDeliveryNum(HttpServletRequest request){
        return R.success(service.getDeliverNum(JwtUtil.getUserId(request.getHeader("token"))));
    }
    /**
     * 投递状态
     */
    @GetMapping("/deliverState")
    public R getDeliverState(HttpServletRequest request){
        return R.success(service.getDelivervo(JwtUtil.getUserId(request.getHeader("token"))));
    }

    /**
     * 找回密码
     * @return
     */
    @GetMapping("/getpassword/{number}")
    public R getPassword(@PathVariable String number){
        return R.success(service.getPassword(number));
    }
    @PostMapping("/regist")
    public R regist(@RequestBody User user){
        return R.success(service.regist(user));
    }
    @PostMapping("/changeName")
    public void changeName(@RequestBody User user){
        service.changeName(user);
    }
    @GetMapping("/feedback")
    public R feedback(HttpServletRequest servletRequest){
        return R.success(service.getDeliverInfo(JwtUtil.getUserId(servletRequest)));
    }
    /**
     * 面试列表
     */
    @GetMapping("/interview")
    public R getInterview(HttpServletRequest servletRequest){
        return R.success(service.getInterviews(JwtUtil.getUserId(servletRequest)));
    }
    @GetMapping("/recentsearch")
    public R getRecentSearch(){
        return R.success(service.getRecentSearch());
    }
    @PostMapping("/search")
    public void search(@RequestBody Map<String, String> map){
        String text=map.get("search");
        service.addSearchRecord(text);
    }
}
