package com.hcb.controllers;

import com.hcb.service.UserService;
import com.hcb.utils.JwtUtil;
import com.hcb.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UploadController {
    @Autowired
    UserService userService;
    @PostMapping("/upload")
    public R upload(MultipartFile file,
                    HttpServletRequest request){
       String url=userService.uploadFile(file, JwtUtil.getUserId(request.getHeader("token")));
        return R.success(url);
    }
    @PostMapping("/uploadImg")
    public R uploadImg(MultipartFile file,
                       HttpServletRequest request){
        return R.success(userService.changeAvater(file,JwtUtil.getUserId(request)));
    }
}
