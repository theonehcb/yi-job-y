package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.Message;

import java.util.List;

public interface MessageMapper extends BaseMapper<Message> {
    public Message getMessagesListItem(Integer userId,Integer id);
    public List<Integer> getUsersId(Integer userId);
    public List<Message> getMessageList(Integer userId,Integer id);
}
