package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.User;
import com.hcb.vo.DeliverVo;
import com.hcb.vo.InterviewList;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {
    public List<DeliverVo> getDeliverVoList(int userId);
    //面试列表
    public List<InterviewList> getInterviews(int id);
}
