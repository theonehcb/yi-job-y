package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.Tags;

public interface TagsMapper extends BaseMapper<Tags> {
}
