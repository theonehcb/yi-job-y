package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.Interview;

public interface InterviewMapper extends BaseMapper<Interview> {
}
