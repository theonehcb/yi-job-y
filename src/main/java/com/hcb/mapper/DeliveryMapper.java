package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.Delivery;
import com.hcb.vo.ApplyVo;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

public interface DeliveryMapper extends BaseMapper<Delivery> {
    public List<Map<String,Object>> feedback(int id);
    //投递列表
    public List<ApplyVo> getApplyVo(int bossId);
    public List<Map<String,Object>> getPassApplyVo(int bossId);
    //投递的jobid
    List<Long> getDeliveryId(int userId);
}
