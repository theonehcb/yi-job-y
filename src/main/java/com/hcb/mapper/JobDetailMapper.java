package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.JobDetail;

public interface JobDetailMapper extends BaseMapper<JobDetail> {
}
