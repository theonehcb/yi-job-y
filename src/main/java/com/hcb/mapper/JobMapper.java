package com.hcb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.pojo.Job;
import com.hcb.vo.ApplyVo;

import java.util.List;

public interface JobMapper extends BaseMapper<Job> {
    public List<Job> getpublish(int bossId);
    //审核
    public void check(int id,int state);

    void publish(int bid,Long jid);
}
