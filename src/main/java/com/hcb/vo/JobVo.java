package com.hcb.vo;

import com.hcb.pojo.Job;
import lombok.Data;

/**
 * @author 11314
 * @description TODO
 * @date 2022/11/9 19:37
 */
@Data
public class JobVo extends Job {
   public boolean visited;
   public JobVo(Job j){
        setJobId(j.getJobId());
        setCompanyName(j.getCompanyName());
        setDegree(j.getDegree());
        setExperience(j.getExperience());
        setLocation(j.getLocation());
        setLogo(j.getLogo());
        setName(j.getName());
        setSalary(j.getSalary());
        setScale(j.getScale());
        setTags(j.getTags());
    }
    public JobVo(){

    }
}
