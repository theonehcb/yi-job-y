package com.hcb.vo;

import lombok.Data;

/**
 * @author 11314
 * @description TODO
 * @date 2022/10/31 14:49
 */
@Data
public class InterviewList {
    String jobName;
    String salary;
    String date;
    String site;
}
