package com.hcb.vo;

import lombok.Data;

/**
 * @author 11314
 * @description TODO
 * @date 2022/10/10 16:00
 */
@Data
public class DeliverVo {
    Integer state;
    String name;
    String logo;
    String companyName;
    String salary;
    String location;
}
