package com.hcb.vo;

import lombok.Data;

@Data
public class ChartVo {
    String imgUrl;
    String message;
    String date;
    //0显示在左边，1在右边
    int position;
}
