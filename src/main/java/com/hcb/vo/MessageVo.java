package com.hcb.vo;

import lombok.Data;


@Data
public class MessageVo {
    Integer userId;
    String imgUrl;
    String username;
    String message;
    String date;
}
