package com.hcb.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 11314
 * @description TODO
 * @date 2022/10/13 11:24
 */

@Data
public class ApplyVo {
    int id;
    String username;
    String jobname;
    int state;
    String pdfurl;
}
