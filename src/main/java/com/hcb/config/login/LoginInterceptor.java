package com.hcb.config.login;

import com.hcb.service.impl.LoginTreadLocalService;
import com.hcb.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 11314
 * @description TODO
 * @date 2022/11/4 19:03
 */
@Component
public class LoginInterceptor implements HandlerInterceptor{
    @Autowired
    LoginTreadLocalService service;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //放行options
        if (HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
            return true;
        }
        int id= JwtUtil.getUserId(request);
        service.setUserId(id);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
       service.remove();
    }
}
